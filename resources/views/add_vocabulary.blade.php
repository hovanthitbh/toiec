
<script src="jquery-3.5.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<div class="logo">
    <a href="{{route('home')}}"><img src="img/LOGO-SOIHOANG01.JPG" height="50"></a>
</div>
<h3 class="text-center">Add new vocabulary</h3>
<span style="float: left;">Number vocabulary: </span><input type="number" id="number" class="form-control col-md-1" value="0" ><br>
<form method="post" action="{{route('addVocabularyPost')}}">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <table class="table table-striped">
        <tr class="text-center">
            <th>No.</th>
            <th>vocalary</th>
            <th>type</th>
            <th>mean</th>
        </tr>
    </table>
    <input type="submit" class="btn btn-success">
</form>
<script>
    $('#number').change(function () {
        $('.row_vocabulary').remove();
        var number = $(this).val();
        for (var i=1 ; i<=number; i++){
            $('table').append("<tr class='row_vocabulary'>\n" +
                "<td>"+i+"</td>"+
                "        <td><input type='text' name='vocabulary[]' class='form-control'></td>\n" +
                "        <td><input type='text' name='type[]' class='form-control'></td>\n" +
                "        <td><input type='text' name='mean[]' class='form-control'></td>\n" +
                "    </tr>");
        }
    });
</script>
@if(session()->has('alert'))
    <script type="text/javascript">
        $(document).ready(function() {
            $.toast({
                heading: '{{Session::get('alert')}}',
                // text: 'Use the predefined ones, or specify a custom position object.',
                position: 'top-right',
                loaderBg: '#3ADF00',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            })
        });
    </script>
@endif

