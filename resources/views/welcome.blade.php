<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="jquery-3.5.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src="plugins/bower_components/toast-master/js/jquery.toast.js"></script>
        <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .list_text{
                visibility:hidden;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="content">
                <div class="title m-b-md">
                    <a href="{{route('home')}}"><img src="img/LOGO-SOIHOANG01.JPG" height="100"></a>
                </div>
                <a href="{{route('test')}}" class="btn btn-success">Start test ({{$total->total}})</a>
                <a href="{{route('addVocabulary')}}" class="btn btn-warning">Add vocabulary</a>
                <button class="btn btn-primary" id="text_again">Start test again</button>
            </div>
            <div class="list_text" id="list_text">
                <table class="table table-striped">
                    <tr><th class="text-center">History test</th>
                        <th class="text-center">Delete</th>
                    </tr
                    @foreach($list_text as $ls)
                        <tr>
                            <td><a href="{{route('testAgain',App\Http\Controllers\HomeController::getID($ls->created_at))}}" >{{$ls->created_at->format('H:i   d.m.Y')}} ( {{ $ls->count }})</a></td>
                            <td class="text-center text-red"><a href="{{route('deleteHistory',App\Http\Controllers\HomeController::getID($ls->created_at))}}"><i class="fa fa-close"></i></a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </body>
    <script>
        $("#text_again").click(function(){
            $(".list_text").removeClass("list_text");
        });
    </script>
    @if(session()->has('alert'))
        <script type="text/javascript">
            $(document).ready(function() {
                $.toast({
                    heading: '{{Session::get('alert')}}',
                    // text: 'Use the predefined ones, or specify a custom position object.',
                    position: 'top-right',
                    loaderBg: '#3ADF00',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                })
            });
        </script>
    @endif
</html>
