<script src="jquery-3.5.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<div class="logo">
    <a href="{{route('home')}}"><img src="img/LOGO-SOIHOANG01.JPG" height="50"></a>
</div>
<h3 class="text-center">Words that you did wrong ({{$count}})</h3>
<table class="table text-center">
    <tr>
        <th>No.</th>
        <th>vocabulary</th>
        <th>type</th>
        <th>means</th>
    </tr>
    @php $i=1; @endphp
    @foreach($va as $va)
        @php if($i==1){$id = $va->id;}@endphp
        <tr>
            <td>{{$i++}}</td>
            <td>{{$va->vocabulary}}</td>
            <td>{{$va->type}}</td>
            <td>{{$va->mean}}</td>
        </tr>
    @endforeach
</table>
<button id="test_all" class="btn btn-warning">Test all vocabulary</button>
<button id="test_wrongs" class="btn btn-primary">Test vocabulary wrongs</button>
<script>
    $('#test_all').click(function () {
        window.open('{{route('test')}}');
        {{--window.location= {{route('test')}};--}}
    });
    $('#test_wrongs').click(function () {
        window.open('{{route('testAgain',$id)}}');
        {{--window.location= {{route('test')}};--}}
    });
</script>
