<script src="jquery-3.5.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<style>
    .hidden{
        visibility:hidden;
    }
</style>
<div class="logo">
    <a href="{{route('home')}}"><img src="{{asset('img/LOGO-SOIHOANG01.JPG')}}" height="50"></a>
</div>
<form method="post" action="{{route('check')}}">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <table width="100%" class="table text-center table-striped">
        <tr>
            <th>No.</th>
            <th>vocabulary</th>
            <th>type</th>
            <th>mean ask</th>
            <th class="hidden">mean</th>
            <th class="hidden">check (not true)</th>
        </tr>
        @php $i=1; @endphp
            @foreach($va as $va)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$va->vocabulary}}</td>
                    <td>{{$va->type}}</td>
                    <td><input id="mean1" type="text" class="form-control" ></td>
                    <td class="hidden" id='mean'>{{$va->mean}}</td>
                    <td class="hidden"><input type="checkbox" name="mean_true[]" value="{{$va->id}}"></td>
                </tr>
            @endforeach
        @if(count($va_new) != 0)
            <tr class="bg-primary text-white"><th colspan="6">New works</th></tr>
            @foreach($va_new as $vn)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$vn->vocabulary}}</td>
                    <td>{{$vn->type}}</td>
                    <td><input id="mean1" type="text" class="form-control" ></td>
                    <td class="hidden" id='mean'>{{$vn->mean}}</td>
                    <td class="hidden"><input type="checkbox" name="mean_true[]" value="{{$vn->id}}"></td>
                </tr>
            @endforeach
        @endif
    </table>
    <span id="display" class="btn btn-success"> Display means</span>
    <input id="save" class="hidden btn btn-primary" value="save" type="submit">
</form>
<script>
    $("#display").click(function(){
        $("td").removeClass("hidden");
        $("th").removeClass("hidden");
        $("#save").removeClass("hidden");
        $("#display").addClass("hidden");

    });
</script>
