<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $list_text = \App\word_wrong::select(DB::raw('count(*) as count, created_at'))
        ->groupBy('created_at') ->orderBy('updated_at', 'DESC')->get();
    $va_total = \App\vocabulary_reading::select(DB::raw('count(id) as total'))->first();
    return view('welcome',['list_text'=>$list_text, 'total'=>$va_total]);
})->name('home');
Route::get('/test','HomeController@test')->name('test');
Route::get('/test-again/{id}','HomeController@test_again')->name('testAgain');
Route::get('/delete-history/{id}','HomeController@delete_history')->name('deleteHistory');
Route::get('/add-vocabulary','HomeController@add_vocabulary')->name('addVocabulary');
Route::post('/add-vocabulary','HomeController@add_vocabulary_post')->name('addVocabularyPost');
Route::post('/test','HomeController@check')->name('check');
