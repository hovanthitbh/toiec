<?php

namespace App\Http\Controllers;

use Dotenv\Validator;
use Illuminate\Http\Request;
use App\vocabulary_reading;
use App\word_wrong;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    function test(){
        $va = vocabulary_reading::inRandomOrder()->get();
        $va_new = array();
        return view('test',['va'=>$va, 'va_new'=>$va_new]);
    }

    function test_again($id){
        $va_wrongs_list = word_wrong::where('id',$id)->first();
        $va_id_list = word_wrong::select('vocabulary_id')
            ->where('created_at','like',$va_wrongs_list->created_at)->get();
        $va = vocabulary_reading::whereIn('id',$va_id_list)->inRandomOrder()->get();
        $va_new = vocabulary_reading::where('created_at', '>', $va_wrongs_list->created_at )->inRandomOrder()->get();
        return view('test',['va'=>$va,'va_new'=>$va_new]);
    }

    function check(Request $request){
        $va_all = vocabulary_reading::all();

        $check = $request->mean_true;
        $count = count($check)."/".count($va_all);
        $va = vocabulary_reading::whereIn('id',$check)->get();
        foreach ($va as $va){
            $word_wrongs = new word_wrong();
            $word_wrongs->vocabulary_id = $va->id;
            $word_wrongs->save();
        }
        $va = vocabulary_reading::whereIn('id',$check)->get();
        return view('check',['va'=>$va,'count'=>$count]);
    }

    function add_vocabulary(){
        return view('add_vocabulary');
    }

    function add_vocabulary_post(Request $request){
        $vocabulary = array_filter($request->vocabulary);
        $type = $request->type;
        $mean = array_filter($request->mean);
        for($i=0; $i<count($vocabulary);$i++){
            $vocabulary_reading = new vocabulary_reading();
            $vocabulary_reading->vocabulary = $vocabulary[$i];
            $vocabulary_reading->type = $type[$i];
            $vocabulary_reading->mean = $mean[$i];
            $vocabulary_reading->save();
        }
        return redirect()->back() ->with('alert', 'You added success');
    }

    function delete_history($id){
        $va_wrongs_list = word_wrong::where('id',$id)->first();
        $va_id_list = word_wrong::select('vocabulary_id')
            ->where('created_at','like',$va_wrongs_list->created_at)->get();
        $va = vocabulary_reading::whereIn('id',$va_id_list)->delete();
        return redirect()->back() ->with('alert', 'You deleted success');
    }

    public static function getID($time){
            $id= \App\word_wrong::where('created_at','like',$time)->first();
            return $id;
    }
}
